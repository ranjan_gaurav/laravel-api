<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;

use Validator;

class Hello extends Controller
{
 public function index()
    {
       //return 'hello world from controller : )';
       
       $users = DB::select('select * from users');  //print_r($users); die();
       
       
       $message = array (
       		'status' => true,
       		'response_code' => '1',
       		'data' => $users
       );
       
       return $message ;
       
    }
    
    private function validator($data)
    {
    	return Validator::make($data, [
    			'user_email' => 'required|email|unique:users',
    			'user_name' => 'required',
    	]);
    }
    
    
  public function create(Request $request)
  {
  	
  	$data = $request->input();  //print_r($data); die();
  	
    $validator = Validator::make($data, [
    			'user_email' => 'required|email|unique:users',
    			'user_name' => 'required',
    	]);
  	
  	if ($validator->fails()) {
  		//$this->setStatusCode(422);
  		$message =  "Not validated";
  	}
  	
  	else {
  		
  	
   
            $data['userid'] = $request->input('userid');
            $data['user_email'] = $request->input('user_email');
            $data['user_pass'] = md5($request->input('user_pass'));
            $data['user_address'] = $request->input('user_address') ? $request->input('user_address') : '';
            $data['user_name'] = $request->input('user_name') ? $request->input('user_name') : '';
            $data['user_nicename'] = $request->input('user_name') ;
            $data['user_mobile'] = $request->input('user_mobile');
            $data['user_dob'] = $request->input('user_dob') ? $request->input('user_dob') : '';
            $data['user_gender'] = $request->input('user_gender') ? $request->input('user_gender') : '';
          //  $data['marital_status'] = $request->input('marital_status') ? $request->input('marital_status') : '';
            //$data['user_url'] = $request->input('user_url') ? $request->input('user_url') : '';
            $data['random_code'] = '234';
            $data['status'] = '1';
        
  	
  	 
  	  DB::table('users')->insert([$data]);
  	
  	$message = array (
  			'status' => true,
  			'response_code' => '1',
  			'message'=> 'User data successfully inserted.',
  			'data' => $data
  	);
  	 
  	
  	 //print_r($data); die();
  	}
  	
  	return $message ;
  	
  	
  	
  	
  	
  }
  
  
  public function update(Request $request)
  {
  	$data = $request->input();  //print_r($data); die();
  
  	$validator = Validator::make($data, [
  			
  			'id' => 'required',
  	]);
  	 
  	if ($validator->fails()) {
  		//$this->setStatusCode(422);
  		$message =  array (
  				'status' => false,
  				'response_code' => '0',
  				'message'=> 'Please Enter UserID',
  				
  		);
  	}
  	
  	else {
  		unset($data['id']);
  		$id = $request->input('id'); 
  		DB::table('users')->where('id',$id)->update($data);
  		
  		$message = array (
  				'status' => true,
  				'response_code' => '1',
  				'message'=> 'User data successfully updated.',
  				
  		);
  		
  		
  	}
  	
  	return $message ;
  	 
  }
  
  
  public function delete(Request $request)
  {
  	$Id = $request->input('id');  //print_r($data); die();
  	
  	$validator = Validator::make($data, [
  				
  			'id' => 'required',
  	]);
  	
  	if ($validator->fails()) {
  		//$this->setStatusCode(422);
  		$message =  array (
  				'status' => false,
  				'response_code' => '0',
  				'message'=> 'Please Enter UserID',
  	
  		);
  	}
  	
  	else 
  	{
  		DB::table('users')->where('id',$id)->delete($data);
  		
  		$message = array (
  				'status' => true,
  				'response_code' => '1',
  				'message'=> 'User data successfully updated.',
  		
  		);
  		
  	}
  }
    
}
