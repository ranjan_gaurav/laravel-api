<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


/* Route::get('/hello',function(){
    return 'Hello World!';
}); */

	//Route::get('hello', 'Hello@index' );
	
	Route::get('/hello','Hello@index',function(){
		return response()->json();
		
	});
	
	Route::get('/create','Hello@create',function(){
			return response()->json();
		
		});
	
	Route::get('/update','Hello@update',function(){
			return response()->json();
		
		});
	
		Route::get('/delete','Hello@update',function(){
			return response()->json();
		
		});